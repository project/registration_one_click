This module is meant to tie in with the [Registration](https://www.drupal.org/project/registration) module, which allows you users to register to entities.

This module defines a function which provides users with a quick link to register to entities without visiting the entity page itself.

*No GUI is provided for this module; it is for developers.*

Usage
-----

 * Install this module
 * Make sure a user can register to, say, node 1
 * In your custom module, add the following code

(add this to your module:)

    ...
    $output = registration_one_click_link($target_user, 'node', 1, t('Register'), t('Cancel your registration'), 'path/to/destination');
    ...

Now, when non-anonymous users will click on this link, they will be registered or unregistered in a single click.

No access checking is done here. If a non-anonymous user can see this link, then they will be able to register, even they do not have permission to do so via the entity page itself. It is developers' responsibility to display this link only when users actually should be able to register or unregister.

The link uses a security token based on:

 * The user's current status within the target entity
 * The target user ID
 * The target user's last login time
 * The currently logged-in user ID
 * The currently logged-in user's last login time, if any
 * The site security hash salt
 * The entity type
 * The entity id.

Links generated for a given user will remain active as long as these don't change.

*Sponsored by CGI.*